import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMaladiesPage } from './list-maladies';

@NgModule({
  declarations: [
    ListMaladiesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMaladiesPage),
  ],
})
export class ListMaladiesPageModule {}
