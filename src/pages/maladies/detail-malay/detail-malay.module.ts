import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailMalayPage } from './detail-malay';

@NgModule({
  declarations: [
    DetailMalayPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMalayPage),
  ],
})
export class DetailMalayPageModule {}
