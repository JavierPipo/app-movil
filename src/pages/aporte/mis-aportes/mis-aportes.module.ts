import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisAportesPage } from './mis-aportes';

@NgModule({
  declarations: [
    MisAportesPage,
  ],
  imports: [
    IonicPageModule.forChild(MisAportesPage),
  ],
})
export class MisAportesPageModule {}
