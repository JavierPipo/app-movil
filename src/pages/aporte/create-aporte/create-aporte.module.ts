import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAportePage } from './create-aporte';

@NgModule({
  declarations: [
    CreateAportePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateAportePage),
  ],
})
export class CreateAportePageModule {}
