import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailAportePage } from './detail-aporte';

@NgModule({
  declarations: [
    DetailAportePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailAportePage),
  ],
})
export class DetailAportePageModule {}
